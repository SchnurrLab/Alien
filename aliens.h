class Alien {
public:
	virtual void init() {
		Serial.println("alien init");
	}
	virtual unsigned int communicate(unsigned int ) {
		delay(1000/50);
		return 0;
	}
};

class Echo: public Alien {
	unsigned char buf[1000/8];
	const int DELAY = 100;
	unsigned int write_i = DELAY;

public:
	void init() {
		this->Alien::init();
		Serial.println("echo init");
		memset(this->buf, 0, sizeof(this->buf));
	}

	unsigned int communicate(unsigned int state) {
		this->Alien::communicate(state);
		//
		this->write_i++;
		this->write_i %= 1000;
		//DEBUG_OUT(write_i);
		//
		unsigned int shift = this->write_i % 8;
		unsigned int byte_i = this->write_i / 8;
		if (state)
			this->buf[byte_i] |= 1 << shift;
		else
			this->buf[byte_i] &= ~(1 << shift);
		//
		byte_i = ((this->write_i+1000) - DELAY) % 1000;
		shift = byte_i % 8;
		return this->buf[byte_i / 8] & (1<<shift);
	}
};

class Counter: public Alien {
	int counter;
	unsigned int laststate;
	unsigned long onTime;
	unsigned long delta;
public:
	void init() {
		this->Alien::init();
		Serial.println("counter init");
		this->counter = this->laststate = this->onTime = this->delta = 0; // wipe
	}
	unsigned int communicate(unsigned int state) {
		this->Alien::communicate(state);
		unsigned long now = millis();
		if (this->laststate != state) { // press or release of the button?
			if (state) { // press?
				if (this->onTime) { // is this a press after the first?
					this->delta = (this->delta + (now - this->onTime)) / 2; // adjust delta
					//DEBUG_OUT(delta); Serial.println(", adjust delta");
				}
				this->onTime = now;
			} else { // release
				this->counter++;
				if (!this->delta) { // are we in the first press state?
					this->delta = (now - this->onTime) * 2; // roughly estimate a delta: double press time
					//DEBUG_OUT(delta); Serial.println(", initial delta");
				}
			}
			this->laststate = state;
		}
		if (!state && this->onTime) { // off but we had a press?
			if (now - this->onTime > 2 * this->delta) { // end of sequence after double wavelength
				Serial.print("end sequence"); DEBUG_OUT(counter); Serial.println();
				this->counter = this->laststate = this->onTime = this->delta = 0; // wipe
			}
		}
		return 0;
	}
};

class Primer: public Alien {
	unsigned int prime;
	unsigned int offset;
	static const unsigned int WAVELENGTH = 100;

	unsigned int isPrime(const unsigned int val) {
		unsigned int sqrti = (int)sqrt((double)val);
		for (unsigned int i=3;i<=sqrti;i+=2)
			if (val % i == 0)
				return 0;
		return 1;
	}

	unsigned int getNext(unsigned int val) {
		val++;
		val |= 1;
		while (!isPrime(val))
			val += 2;
		return val;
	}
public:
	void init() {
		this->Alien::init();
		Serial.println("primer init");
		this->prime = 2; // reset
		this->offset = 1;
	}

	unsigned int communicate(unsigned int state) {
		unsigned int ret = 0;
		if (state) {
			Serial.println("button press");
			this->prime = 2; // reset
			this->offset = 1;
		} else
			if (this->offset) { // pulse out number
				unsigned int len = this->prime * WAVELENGTH;
				unsigned int dur = len + 4*WAVELENGTH;
				DEBUG_OUT(dur); DEBUG_OUT(len); DEBUG_OUT(offset);
				if (this->offset < dur) { // are we pulsing or waiting inbetween?
					Serial.println(", pulsing");
					if (offset < len) { // are we pulsing?
						if (offset%WAVELENGTH < WAVELENGTH/2) { // are we in on-state?
							ret = 1;
						}
					}
					this->offset++;
				} else {
					Serial.println(", next");
					this->offset = 0; // trigger next, we are done pusling
				}
			} else {
				Serial.println("start");
				this->prime = this->getNext(this->prime); // next
				this->offset = 1;
			}
		return ret;
	}
};

class YesNo: public Alien {
	int toggle;
	unsigned int offset;
	static const unsigned int WAVELENGTH = 4;
	unsigned int LEN = 8;
	static const unsigned int YES = 0b0011100111;
	static const unsigned int NO = 0b01010101;
	unsigned int bitcount(unsigned int mask) {
		unsigned int count = 1;
		while (mask) {
			count++;
			mask >>= 1;
		}
		return count;
	}
public:
	void init() {
		this->Alien::init();
		Serial.println("yesno init");
		LEN = max(bitcount(YES), bitcount(NO));
	}

	unsigned int communicate(unsigned int state) {
		unsigned int ret = 0;
		this->Alien::communicate(state);
		if (state) {
			Serial.println("button press");
			this->offset = 1;
			this->toggle = !this->toggle;
		} else if (this->offset) {
			if (this->offset < LEN*WAVELENGTH) {
				ret = (this->toggle?YES:NO) & (1 << (this->offset / WAVELENGTH));
			}
			this->offset++;
			if (this->offset >= LEN*WAVELENGTH) {
				this->offset = 0;
				ret = 0;
			}
		}
		return ret;
	}
};
