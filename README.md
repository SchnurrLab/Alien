Alien
==

> Wenn ich etwas sage, wird es dann auch so verstanden, wie ich es gemeint habe?

> Was, wenn das Gegenüber eine ganz andere Perspektive hat, eine ganz andere Sicht und Verständnis der Dinge?

> Wie kommuniziere ich, wenn ich auf die gewohnten Kanäle verzichten muss? Was bleiben für Möglichkeiten, Kontakt aufzunehmen und Informationen zu tauschen?

Dies ist das Alien-Projekt vom Schnurrlab. Es geht um nichts als Kommunikation. Das Alien hat einen Eingangskanal und einen Ausgangskanal. Der Eingangskanal, der ein Taster oder ein Lichtsensor sein kann, kann entweder "Ein" oder "Aus" sein. Genauso der Ausgangskanal, der eine Leuchdiode ist, die entweder leuchtet oder auch nicht. Was kann damit kommuniziert werden? Was steckt im Alien und was will es mir sagen? Und was kann ich dem Alien sagen und was antwortet es?

Stoff-Alien
--

![Stoffmodel](IMG_20220222_124024s.jpg)

3D-Alien
--

![Body](alien.png)

Eingangskanal Varianten
--

Die Eingangskanäle sind in `readers.h`:

- Debouncer: ein entprellender Tastenleser
- Shadower: ein Mittelwert berechnender Lichstsensor, der Abschattung bemerkt

Die verschieden kommunizierenden Aliens sind in `aliens.h`. Es sind Module, die einen Zustand von einem Eingangskanal bekommen und einen Ausgangszustand zurückgeben. Sie sind also die Kommunikationslogik eines Alien:

- Echo: ein Alien, das das Signal nach einer Zeit echot.
- Counter: ein Alien, das Zählt die Häufigkeit des Signals.
- Primer: ein Alien, das Primzahlen ausgibt

In den Alien steckt also Code, um Zahlen ein- (in Counter) und auszugeben (in Primer). Damit lässt sich was beginnen...

etc, etc
--

![ci](https://ci.codeberg.org/api/badges/SchnurrLab/Alien/status.svg)

Alles hier steht entweder unter der " [GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007](LICENSE) " (Programmsourcecode) oder " [CC BY-CA](https://creativecommons.org/licenses/by-sa/4.0/) " (Kreativcode).

