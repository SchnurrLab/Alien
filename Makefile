#!/usr/bin/make -f
.SUFFIXES: .scad .stl .png .gif
.PHONY: all png stl clean
OSFLAGS=--imgsize 1440,720 --colorscheme Solarized --viewall --autocenter

.scad.stl:
	openscad -o $@ $<

.scad.png:
	openscad --camera -700,700,700,0,0,0 $(OSFLAGS) -o $@ $<

all: build/alien.ino.hex
	test -c /dev/ttyUSB0 && \
	avrdude \
		-C/etc/avrdude.conf \
		-v \
		-patmega328p \
		-carduino \
		-P/dev/ttyUSB0 \
		-b57600 \
		-D \
		-Uflash:w:build/alien.ino.hex:i || \
	true

run: alien.stl alien.png
	pyserial-miniterm /dev/ttyUSB0 115200 --raw --eol CR --rts 0

dbg:
	git push

clean:
	rm -rf build/ cache/ libraries/ packages/ *.stl

build/alien.ino.hex:
	mkdir -p build cache libraries packages
	arduino-builder \
		-compile \
		-hardware /usr/share/arduino/hardware \
		-hardware `pwd`/packages \
		-tools /usr/share/arduino/hardware/tools/avr \
		-tools `pwd`/packages \
		-libraries `pwd`/libraries \
		-fqbn=arduino:avr:nano:cpu=atmega328old \
		-vid-pid=1A86_7523 \
		-ide-version=10819 \
		-build-path `pwd`/build \
		-warnings=all \
		-build-cache `pwd`/cache \
		-prefs=build.warn_data_percentage=75 \
		-verbose `pwd`/alien.ino
