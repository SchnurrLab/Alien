$fn = 80;

module schwanz(koerperr,koerperw,schnorchelr,schnorcheld) {
	// schwanz
	rotate([180,0,0])
		translate([0,0,koerperr * 2 + .1])
			difference() {
				union() {
					// zylinder
					minkowski() {
						cylinder(koerperw*4,koerperw,0);
						sphere(koerperr);
					}
					// lego knopf
					translate([0,0,-koerperr-2])
						cylinder(2, koerperw-2, koerperw-2+.1);
				}
				union() {
					// zylinder innen
					translate([0,0,2])
						minkowski() {
							cylinder(koerperw*4-6,koerperw-2,0);
							sphere(koerperr);
						}
					translate([0,0,-koerperr*3])
						cylinder(koerperr*6, koerperw-4, koerperw-4);
				}
			}
}

module koerper(koerperr,koerperw,schnorchelr,schnorcheld) {
	// koerper
	difference() {
		union() {
			// lego knopf
			translate([0,0,koerperr])
				cylinder(2, koerperw-2, koerperw-2+.1);
			difference() {
				// koerper
				minkowski() {
					cylinder(.000001, koerperw, koerperw);
					sphere(koerperr);
				}
				// lego loch
				translate([0,0,-koerperr])
					cylinder(2, koerperw-2, koerperw-2+.1);
			}
			// schnorchel
			translate([0,koerperr+koerperw/2,schnorchelr])
				rotate([0,90,0]) {
					rotate_extrude(angle=80, convexity=10)
						translate([schnorchelr, 0])
							circle(schnorcheld);
								rotate(80)
					// saugnapf am schnorchel
					translate([schnorchelr,0,0])
						sphere(schnorcheld*2);
				}
		}
		// hohlraeume
		union() {
			// hohlraum im koerper
			translate([0,0,-koerperr*3])
				cylinder(koerperr*6, koerperw-4, koerperw-4);
			minkowski() {
				cylinder(.000001, koerperw-4, koerperw-4);
				sphere(koerperr-2);
			}
			// hohlraum im schnorchel
			translate([0,koerperr+koerperw/2,schnorchelr])
				rotate([0,90,0]) {
					rotate_extrude(angle=90, convexity=10)
						translate([schnorchelr, 0])
							circle(schnorcheld-2);
					// saugnapf am schnorchel
					rotate(80)
						translate([schnorchelr,0,0])
							union() {
								translate([-20,0,-20])
									cube(40);
								sphere(schnorcheld*2-2);
							}
				}
		}
	}
}

module kopf(koerperr,koerperw,schnorchelr,schnorcheld) {
	// kopf
		difference() {
			union() {
				// zylinder
				minkowski() {
					cylinder(koerperw*2,koerperw,0);
					sphere(koerperr);
				}
				translate([-schnorchelr,0,koerperw*2])
					rotate([90,0,0]) {
						rotate_extrude(angle=80, convexity=10)
							translate([schnorchelr, 0])
								circle(schnorcheld);
									rotate(80)
						// saugnapf am schnorchel
						translate([schnorchelr,0,0])
							sphere(schnorcheld*2);
					}

			}
			union() {
				// zylinder innen
				translate([0,0,2])
					minkowski() {
						cylinder(koerperw*2-6,koerperw-2,0);
						sphere(koerperr);
					}
				translate([0,0,-koerperr*3])
					cylinder(koerperr*6, koerperw-4, koerperw-4);
				// lego knopf
				translate([0,0,-koerperr])
					cylinder(2, koerperw-2, koerperw-2+.1);
				translate([-schnorchelr,0,koerperw*2])
					rotate([90,0,0]) {
						rotate_extrude(angle=95, convexity=10)
							translate([schnorchelr, 0])
								circle(schnorcheld-2);
					}
			}
		}
}

module alien(s,koerperr,koerperw,schnorchelr,schnorcheld,bodycount) {
	scale([s,s,s]) {
		schwanz(koerperr,koerperw,schnorchelr,schnorcheld);
		// koerper
		for (i=[0:1:bodycount-1])
			translate([0,0,koerperr*2*i+i])
				rotate([0,0,140*i])
					koerper(koerperr,koerperw,schnorchelr,schnorcheld);
		translate([0,0,koerperr*2*bodycount+bodycount])
			kopf(koerperr,koerperw,schnorchelr,schnorcheld);
	}
}

let(koerperr=8,koerperw=20,schnorchelr=60,schnorcheld=6) {
	//alien(4,koerperr,koerperw,schnorchelr,schnorcheld,8);
	translate([3*koerperw,0,0])
		rotate([0,0,90])
			kopf(koerperr,koerperw,schnorchelr,schnorcheld);
	translate([3*koerperw,3*koerperw,0])
		koerper(koerperr,koerperw,schnorchelr,schnorcheld);
	translate([0,3*koerperw,0])
		koerper(koerperr,koerperw,schnorchelr,schnorcheld);
	translate([0,0,-koerperr*2+2])
		rotate([0,180,0])
			schwanz(koerperr,koerperw,schnorchelr,schnorcheld);
}
