#define DEBUG_OUT(var) Serial.print(", " #var "="); Serial.print(var);
// pins:
static const int D2 = 2;
static const int D3 = 3;
//

#include "aliens.h"
#include "readers.h"

static Echo echo;
static Counter counter;
static Primer primer;
static YesNo yesno;
static Alien* aliens[] = {
	&echo,
	&counter,
	&primer,
	&yesno,
	NULL
};
static Alien* alien = aliens[3];
static Debouncer input; // debounced switch input
// static Shadower input; // light sensor input

void setup() {
	// debug
	Serial.begin(115200);
	Serial.println("setup");
	// if switch is used its connected to D3
	input.init(D3); // Debouncer
	// if light sensor is used its connected to A0
	// input.init(A0); // Shadower
	// debug pin which shows if buttonpress/shadow was detected
	pinMode(LED_BUILTIN, OUTPUT);
	// comm led is at D2
	pinMode(D2, OUTPUT);
	digitalWrite(D2, LOW);
	//
	for (int i=0;aliens[i];i++) {
		aliens[i]->init();
	}
}

void loop() {
	unsigned int state = 0;
	// determine the current state:
	state = input.read();
	// record the state:
	//DEBUG_OUT(state);
	digitalWrite(LED_BUILTIN, state?HIGH:LOW); // switch control led
	//
	// the the aliens answer state:
	state = alien->communicate(state);
	digitalWrite(D2, state?HIGH:LOW); // switch comm
	// finish:
}

// vim:path=/usr/lib/avr/include
