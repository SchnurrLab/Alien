class Reader {
protected:
	int pin;
public:
	virtual void init(int pin) {
		Serial.println("reader init");
		this->pin = pin;
	}
	virtual int read() {
		return 0;
	}
};

class Debouncer: public Reader {
	static const int DELTA = 6;
	int level;
public:
	void init(int pin) {
		this->Reader::init(pin);
		Serial.println("debouncer init");
		this->level = 0;
		pinMode(this->pin, INPUT_PULLUP);
	}
	int read() {
		if (digitalRead(this->pin) == HIGH) { // HIGH means off / released
			if (this->level>0) this->level--;
		} else {
			if (this->level<DELTA-1) this->level++;
		}
		return level >= DELTA/2;
	}
};

class Shadower: public Reader {
	static const int DELTA = 30;
	double average;
public:
	void init(int pin) {
		this->Reader::init(pin);
		Serial.println("shadower init");
		this->average = 700;
		pinMode(this->pin, INPUT);
	}
	int read() {
		int sensor_value = analogRead(this->pin);
		DEBUG_OUT(sensor_value);
		if (sensor_value < average / 3) {
			return 1;
		}
		else {
			average = (average * (DELTA-1) + sensor_value) / DELTA;
		}
		DEBUG_OUT(average);
		return 0;
	}
};

